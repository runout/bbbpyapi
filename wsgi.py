#!/usr/bin/python
''' main module for usage with uwsgi
'''

__author__ = 'Markus Gschwendt'
__copyright__ = 'See the file README for further information'
__license__ = 'See the file LICENSE for further information'

from typing import Optional, Union
from lib.bbbmeetings import *
from lib.bbbmeeting import endMeetingUri, joinMeetingUri
from lib.bbbstats import getStats, TableFormatter

def application(environ, start_response):
    ''' application function for uwsgi

    '''

    stats = getStats(fmt='e_html', tableFormatter=MyTableFormatter())
    if not stats:
        stats = 'Sorry, no meetings at the moment.'

    htmlResponse = [ "<!DOCTYPE html>" ]
    htmlResponse.append("<html>")
    htmlResponse.append("<head>")
    htmlResponse.append("<meta charset=\"utf-8\">")
    htmlResponse.append("<meta http-equiv=\"refresh\" content=\"60\">")
    htmlResponse.append("<title>BBB Statistik Dr. Roland</title>")
    htmlResponse.append("</head>")
    htmlResponse.append("<body>")
    htmlResponse.append(stats)
    htmlResponse.append("</body>")
    htmlResponse.append("</html>")
    start_response('200 OK', [('Content-Type', 'text/html')])
    return [ line.encode("utf-8") for line in htmlResponse ]


class MyTableFormatter(TableFormatter):
    def cellFormatter(self, cellContent: Union[int, str], colName: str, cellStyle: str, fmt: str, params: Optional[dict] = None) -> Union[int, str]:
        ''' Formatter for cell contents.

        :param cellContent: content/value of the cell
        :type cellContent: string
        :param colName: name of column to format
        :type colName: string
        :param fmt: type of output format
        :type fmt: string
        :param params: dictionary of parameters needed for rendering
        :type params: dict
        :returns: formatted cell
        :rtype: int, string

        '''

        newCellContent = cellContent
        if fmt == 'e_html':
            style: str = ''
            if cellStyle == 'footer':
                 style = style + 'border-top: 1px solid black; '
            if isinstance(cellContent, int):
                 style = style + 'text-align: right; '
                 cellContent = str(cellContent)

            newCellContent = '<td>' + cellContent + '</td>'
            # cell of header line
            if cellStyle == 'header':
                newCellContent = '<th>' + cellContent + '</th>'
            else:
                if style != '':
                     style = ' style="' + style + '"'

                if colName == 'moderator names':
                    cellContent = cellContent.replace(';', '<br />')

                elif colName == 'duration' and cellStyle == 'footer':
                    cellContent = '<td' + style + '></td>'

                elif colName == 'join meeting':
                    if params['meetingID']:
                        uriStr = joinMeetingUri(params['meetingID'], 'moderator', 'IT Admin')
                        cellContent = '<a href="' + uriStr  + '" target="_blank">' + cellContent + '</a>'
                    else:
                        cellContent = ''

                elif colName == 'end meeting':
                    if params['meetingID']:
                        uriStr = endMeetingUri(params['meetingID'])
                        cellContent = '<a href="' + uriStr  + '">' + cellContent + '</a>'
                    else:
                        cellContent = ''

                newCellContent = '<td' + style + '>' + cellContent + '</td>'

        return newCellContent


    def tableFormatter(self, tableContent: list, fmt: str, params: Optional[dict] = None) -> Union[list, str]:
        ''' Formatter for table contents.

        :param tableContent: list of rows for the table
        :type tableContent: list
        :param fmt: type of output format
        :type fmt: string
        :param params: dictionary of parameters needed for rendering
        :type params: dict
        :returns: formatted table
        :rtype: list, string

        '''

        newTableContent: Union[list, str] = tableContent

        if fmt == 'e_html':
            style = ' style="background-color: #eeeeee; "'
            newTableContent = ''.join(_row for _row in tableContent)
            newTableContent = '<table' + style + '>' + newTableContent + '</table>'

        return newTableContent

