#!/usr/bin/python
''' main module for bbbpyapi
This module imports the most importent functions
'''

__author__ = 'Markus Gschwendt'
__copyright__ = 'See the file README.md for further information'
__license__ = 'See the file LICENSE for further information'

from lib.bbbmeetings import getMeetingsEtree, \
                            getMeetingsList, \
                            getMeetingsXml
from lib.bbbmeeting import  findMeeting, \
                            getMeetingAttendees, \
                            getMeetingID, \
                            getMeetingInfoEtree, \
                            getMeetingInfoXml, \
                            getMeetingModPWD, \
                            getMeetingModerators, \
                            getMeetingStats, \
                            endMeeting
from lib.bbbstats import getStats
from lib.bbbapi import bbbApiRequest
from lib.config import bbbdomain, BBBSECRET
def getMeetings():
    return getMeetingsList()
