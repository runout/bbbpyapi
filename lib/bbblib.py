#!/usr/bin/python
''' Common functions for the bbbstats project
'''

__author__ = 'Markus Gschwendt'
__copyright__ = 'See the file README for further information'
__license__ = 'See the file LICENSE for further information'

from typing import Optional
import time
import re
from hashlib import md5

'''
import uwsgi is only possible if uwsgi is running
if called from console use timestamp instead as cacheid
'''

try:
    import uwsgi # type: ignore
except ImportError:
    ''' this is used to determine if we are running on uwsgi or being called from command line
    '''

    uwsgi = None

# timestamp when program was executed (or uwsgi was started -> not start time from request!)
timestamp_ms = int(round(time.time() * 1000))

def cachefunc(*args, **kwargs):
    ''' Unique CacheID
    As this function is used by @cached decorators from cachetools
    it must accept the same arguments as the decorated function

    :param args: must be convertable to strings
    :param kwargs: must be convertable to strings
    :returns: md5 string based on the arguments given to the decorated function and the request/session
    :rtype: string

    '''

    argstr = ' '.join(str(_)[0:100] for _ in args) + ''.join(str(_)[0:100] for _ in kwargs.values()) + str(timestamp_ms)

    if uwsgi:
        # if we are running on uwsgi
        argstr = argstr + str(uwsgi.worker_id()) + '-' + str(uwsgi.request_id())
    return md5(argstr.encode('utf-8')).hexdigest()


def validateMeetingID(meetingID: Optional[str] = None) -> bool:
    ''' Test meetingID for valid characters

    :param meetingID: meetingID to check
    :type meetingID: str
    :returns: True/False
    :rtype: bool

    '''

    if meetingID:
        _search = re.compile(r'[^a-zA-Z0-9\-\_]')
        return not bool(_search.search(meetingID))

    return False
