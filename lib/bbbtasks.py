#!/usr/bin/python
''' Tasks for BBB meetings
'''

__author__ = 'Markus Gschwendt'
__copyright__ = 'See the file README for further information'
__license__ = 'See the file LICENSE for further information'

from typing import Union
from os import path, mkdir
import pickle
from .config import bbbdomain, timeMaxUnmoderated, fileWatchUnmoderated
from .bbblib import timestamp_ms
from .bbbmeetings import getMeetingsList
from .bbbmeeting import getMeetingID, getMeetingModerators, endMeeting


def _yieldUnmoderated(unmoderatedMeetings: list, bbbdomain: str = bbbdomain):
    ''' Generator function for unmoderated meetings

    :param unmoderatedMeetings: list of dictionaries with meetings
    :type unmoderatedMeetings: list
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: Unmoderated meetings by yield
    :rtype: dict

    '''

    for _u in unmoderatedMeetings:
        if _u and 0 < len(_u):
            moderators = getMeetingModerators(_u['internalMeetingID'], bbbdomain=bbbdomain)
            if not (moderators) or (len(moderators) == 0):
                yield _u


def _unmodTimedOut(watchlist: list, internalMeetingID: str) -> Union[bool, dict]:
    ''' Check if a given ID is already watched and if timeout is reached

    :param watchlist: list of meetings which are already on the watchlist
    :type watchlist: list
    :param internalMeetingID: Internal meeting ID to search for
    :type internalMeetingID:  str
    :returns: status of given ID in list or the found watchlist entry if the timeout is not reached
    :rtype: bool, dict

    '''

    for _w in watchlist:

        if internalMeetingID == _w['internalMeetingID']:
            if timestamp_ms > _w['timestamp'] + _w['timeout'] * 1000:

                ''' check if unmoderated for longer then timeMaxUnmoderated
                then end the meeting
                '''
                return True

            return _w

    return False

def cleanupUnmoderated(timeOut: int = timeMaxUnmoderated, bbbdomain: str = bbbdomain) -> str:
    ''' End meetings which are without a moderator for an amount of time.
    if no timeout is given, the default is _timeMaxUnmoderated_ from config

    :param timeout: time in seconds a meeting is allowed to be unmoderated
    :type timeout: int
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: response from the BBB server as XML
    :rtype: string

    '''

    retXML = ''
    watchlistNew = []

    # load watchlist
    try:
        watchlist = pickle.Unpickler(open(fileWatchUnmoderated, 'rb')).load()
    except:
        watchlist = []

    for _u in _yieldUnmoderated(getMeetingsList(bbbdomain=bbbdomain), bbbdomain=bbbdomain):
 
        _uTO = _unmodTimedOut(watchlist, _u['internalMeetingID'])
        if _uTO == True:
            ''' if unwatched meeting is already in watchlist and timeout is reached
            end meeting and append result string to previous results '''
            retXML = retXML + str(endMeeting(_u['internalMeetingID'], bbbdomain=bbbdomain))
        else:
            try:
                ''' timestamp from watchfile if we found an entry '''
                _ts = _uTO['timestamp'] # type: ignore
                _to = _uTO['timeout'] # type: ignore
            except:
                ''' fallback. timestamp from config file '''
                _ts = timestamp_ms
                _to = timeOut
 
            watchlistNew.append({'internalMeetingID': _u['internalMeetingID'],
                                           'timestamp': _ts,
                                           'timeout': _to,
                                           'meetingID': getMeetingID(_u['internalMeetingID'], bbbdomain=bbbdomain)})
        
    if not path.isdir(path.dirname(path.abspath(fileWatchUnmoderated))):
        mkdir(path.dirname(path.abspath(fileWatchUnmoderated)))

    pickle.dump(watchlistNew, open(fileWatchUnmoderated, 'wb'))

    return retXML

