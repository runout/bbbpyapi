#!/usr/bin/python
''' Functions related to Lists of BBB recordings
'''

__author__ = 'Markus Gschwendt'
__copyright__ = 'See the file README for further information'
__license__ = 'See the file LICENSE for further information'

from typing import Optional
from cachetools import *
from .bbblib import cachefunc
from .config import bbbdomain
from .bbbapi import bbbApiRequest, bbbApiParamsDict2Str
from .bbbparse import *
#from .bbbmeeting import 

_cacheRL: LRUCache = LRUCache(maxsize=16)
@cached(_cacheRL, key=cachefunc)
def getRecordingsList(etree=None, bbbdomain: str = bbbdomain):
    ''' List of recordings

    :param etree: etree object with meetins. If none, we try to get it from server.
    :type request_type: object
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: list of recordings (as dict)
    :rtype: list

    '''

    if not etree:
        etree = getRecordingsEtree(bbbdomain=bbbdomain)
    return etreeRecordingsList(etree)


_cacheRX: LRUCache = LRUCache(maxsize=16)
@cached(_cacheRX, key=cachefunc)
def getRecordingsXml(params: Optional[dict] = None, bbbdomain=bbbdomain) -> Optional[str]:
    ''' List of recordings as XML

    :param params: Dictionary of parameters for the BBB API request as specified by the BBB API
    :type params: None,dict
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: XML with recordings
    :rtype: None,string

    '''

    res = bbbApiRequest('getRecordings', bbbApiParamsDict2Str(params), bbbdomain=bbbdomain)
    if res:
        return etreeIndent(res['etree'])
    return None


_cacheRE: LRUCache = LRUCache(maxsize=16)
@cached(_cacheRE, key=cachefunc)
def getRecordingsEtree(params: Optional[dict] = None, bbbdomain: str = bbbdomain):
    ''' List of recordings as tree object

    :param params: Dictionary of parameters for the BBB API request as specified by the BBB API
    :type params: None,dict
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: etree object with meetings
    :rtype: object

    '''

    res = bbbApiRequest('getRecordings', bbbApiParamsDict2Str(params), bbbdomain=bbbdomain)
    if res:
        return res['etree']
    return None
