#!/usr/bin/python
''' Functions for doing requests to the API of a BBB server
https://docs.bigbluebutton.org/dev/api.html
'''

__author__ = 'Markus Gschwendt'
__copyright__ = 'See the file README for further information'
__license__ = 'See the file LICENSE for further information'

import subprocess
import hashlib
from typing import Optional
from requests import get as request_get
from urllib.parse import urlencode, quote_plus
import re
import sys
import time
from cachetools import *
from requests.exceptions import HTTPError, ConnectionError
from .config import bbbdomain, BBBSECRET
from .bbblib import cachefunc
from .bbbparse import etreeParse, etreeStatus

class Error(Exception):
    pass

class BbbApiError(Error):
    pass

def _getChecksum(request):
    ''' Construct a checksum for API requests to a BBB server
    if there is no BBBSECRET defined in the config file we will try to get the secret from the BBB server if its running on the local machine

    :param request: request string for the BBB API
    :type request: string
    :returns: hash for the given request
    :rtype: string

    '''

    if BBBSECRET:
        bbb_secret = BBBSECRET
    else:
        # get API Secret from bbb-conf command (as byte string, newer python has argument 'text=True')
        bbb_secret = subprocess.run("bbb-conf --secret",
                                   shell=True,
                                   stdout=subprocess.PIPE).stdout.decode('utf-8').strip()
        bbb_secret = re.search('^\s{4}Secret:\s(.*)$', bbb_secret, re.MULTILINE).group(1)
    bbb_request = request + bbb_secret
    # return the checksum as SHA1 hash
    return hashlib.sha1(bbb_request.encode('utf-8')).hexdigest()

def bbbApiParamsDict2Str(params: Optional[dict]) -> str:
    ''' Convert a dictionary of request parameters
    and urlencode it

    :param params: request parameters
    :type params: None, dict
    :returns: parameters for using in URL
    :rtype: str

    '''

    paramStr = ''

    if params:
        paramStr = urlencode(params, quote_via=quote_plus) # type: ignore

    return paramStr


def bbbApiRequestUri(request_type: str, request_params: str, bbbdomain: str = bbbdomain) -> str:
    ''' Constructs the URI for a request to the BBB server API

    :param request_type: Type of the API call
    :type request_type: string
    :param: request_params: Request parameters for the call or empty string
    :type request_params: string
    :param bbbdomain: Domain name of the BBB server, defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: URI for an API request
    :rtype: str

    '''

    checksum: str = _getChecksum(request_type + request_params)
    if request_params:
        request_params = request_params + '&'

    return 'https://' + bbbdomain + '/bigbluebutton/api/' + request_type + '?' + request_params + 'checksum=' + checksum


@cached(LRUCache(maxsize=8), key=cachefunc)
def bbbApiRequest(request_type: str, request_params: str, bbbdomain: str = bbbdomain) -> dict:
    ''' Sends a request to the BBB server API and parses the response

    :param request_type: Type of the API call
    :type request_type: string
    :param: request_params: Request parameters for the call or empty string
    :type request_params: string
    :param bbbdomain: Domain name of the BBB server, defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: Dictionary with the keys 'apistatus', 'etree', 'xml'
    :rtype: dict

    '''

    request_uri = bbbApiRequestUri(request_type, request_params, bbbdomain = bbbdomain)
    try:
        res = request_get(request_uri)
        res.raise_for_status()
        res_etree = etreeParse(res.text)
        res_status = etreeStatus(res_etree)
    except HTTPError as err:
        sys.exit('HTTP error occurred: ' +  str(err))
        return {'apistatus': err, 'etree': None}
    except ConnectionError as err:
        sys.exit('Connection error occurred: ' +  str(err))
        return {'apistatus': err, 'etree': None}
    else:
        if res_status and res_status['returncode'] != 'SUCCESS':
            print('BBB API request was not successful: ' + res_status['returncode'] + ' ' + str(res_status['messageKey']) + ' ' + str(res_status['message']))

    return {'apistatus': res_status, 'etree': res_etree, 'xml': res.text}
