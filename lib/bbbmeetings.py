#!/usr/bin/python
''' Functions related to Lists of BBB meetings
'''

__author__ = 'Markus Gschwendt'
__copyright__ = 'See the file README for further information'
__license__ = 'See the file LICENSE for further information'

from os import path, mkdir
import pickle
from cachetools import *
from .bbblib import cachefunc
from .config import bbbdomain, timeMaxUnmoderated, fileWatchUnmoderated
from .bbbapi import bbbApiRequest
from .bbbparse import *
#from .bbbmeeting import 

_cacheML: LRUCache = LRUCache(maxsize=16)
@cached(_cacheML, key=cachefunc)
def getMeetingsList(etree=None, bbbdomain: str = bbbdomain):
    ''' List of meetings

    :param etree: etree object with meetins. If none, we try to get it from server.
    :type request_type: object
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: list of meetings (as dict)
    :rtype: list

    '''

    if not etree:
        etree = getMeetingsEtree(bbbdomain=bbbdomain)
    return etreeMeetingsList(etree)


_cacheMX: LRUCache = LRUCache(maxsize=16)
@cached(_cacheMX, key=cachefunc)
def getMeetingsXml(bbbdomain: str = bbbdomain):
    ''' List of meetings as XML

    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: XML with meetings
    :rtype: string

    '''

    return etreeIndent(bbbApiRequest('getMeetings', '', bbbdomain=bbbdomain)['etree'])


_cacheME: LRUCache = LRUCache(maxsize=16)
@cached(_cacheME, key=cachefunc)
def getMeetingsEtree(bbbdomain: str = bbbdomain):
    ''' List of meetings as tree object

    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: etree object with meetings
    :rtype: object

    '''

    return bbbApiRequest('getMeetings', '', bbbdomain=bbbdomain)['etree']
