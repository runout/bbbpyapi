#!/usr/bin/python
''' Functions related to BBB meetings
'''

__author__ = 'Markus Gschwendt'
__copyright__ = 'See the file README for further information'
__license__ = 'See the file LICENSE for further information'

from typing import Optional
from datetime import datetime
from dateutil import parser as dateparser
import re
from uuid import uuid4
from cachetools import *
from .config import bbbdomain, joinParamsDefaults, createParamsDefaults
from .bbblib import cachefunc, validateMeetingID
from .bbbapi import bbbApiRequest, bbbApiRequestUri, bbbApiParamsDict2Str
from .bbbparse import *
from .bbbmeetings import getMeetingsList, getMeetingsEtree

_cacheMIX: LRUCache = LRUCache(maxsize=16)
@cached(_cacheMIX, key=cachefunc)
def getMeetingInfoXml(meetingID: str, bbbdomain: str = bbbdomain) -> Optional[str]:
    ''' Get information for specified meeting as XML

    :param meetingID: meetingID of the requestd meeting
    :type request_type: string
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: XML for specified meeting
    :rtype: string

    '''

    res = bbbApiRequest('getMeetingInfo', 'meetingID=' + meetingID, bbbdomain)
    if res:
        return res['xml']
    return None

_cacheMIE: LRUCache = LRUCache(maxsize=16)
@cached(_cacheMIE, key=cachefunc)
def getMeetingInfoEtree(meetingID: str, bbbdomain: str = bbbdomain):
    ''' Get information for specified meeting as etree object

    :param meetingID: meetingID of the requestd meeting
    :type request_type: string
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: etree object for specified meeting
    :rtype: object

    '''

    res = bbbApiRequest('getMeetingInfo', 'meetingID=' + meetingID, bbbdomain=bbbdomain)
    if res:
        return res['etree']
    return None

def getMeetingStats(internalMeetingID: str, bbbdomain: str = bbbdomain) -> Optional[dict]:
    ''' returns statistics for a meeting as dictionary
    count attendees
    count moderators
    count audio joined
    count microphon joined
    count webcam joined
    minutes running
    recording on/off

    :param internalMeetingID: internalMeetingID of the meeting
    :type internalMeetingID: string
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: meeting statistics
    :rtype: dict

    '''

    etreeMeetings = getMeetingsEtree(bbbdomain=bbbdomain)
    d = int(datetime.now().strftime('%s'))
    origin = etreeOrigin(etreeMeetings, internalMeetingID)
    meetingName = etreeCount(etreeMeetings, internalMeetingID, 'meetingName')
    cntAttendees = int(etreeCount(etreeMeetings, internalMeetingID, 'participantCount'))
    cntListenOnly = int(etreeCount(etreeMeetings, internalMeetingID, 'listenerCount'))
    cntVoice = int(etreeCount(etreeMeetings, internalMeetingID, 'voiceParticipantCount'))
    cntModerators = int(etreeCount(etreeMeetings, internalMeetingID, 'moderatorCount'))
    cntVideo = int(etreeCount(etreeMeetings, internalMeetingID, 'videoCount'))
    cntDuration = int(d - int(dateparser.parse(etreeCount(etreeMeetings, internalMeetingID, 'createDate')).timestamp()))
    return {'origin': origin,
            'meetingName': meetingName,
            'attendees': cntAttendees,
            'listenonly': cntListenOnly,
            'voice': cntVoice,
            'moderators': cntModerators,
            'video': cntVideo,
            'duration': cntDuration}

_cacheMID: LRUCache = LRUCache(maxsize=256)
@cached(_cacheMID, key=cachefunc)
def getMeetingID(internalMeetingID: str, bbbdomain: str = bbbdomain) -> Optional[str]:
    ''' Lookup the meetingID for a given internalMeetingID

    :param internalMeetingID: internalMeetingID of the meeting
    :type internalMeetingID: string
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: meetingID
    :rtype: string

    '''

    return etreeMeetingTag(getMeetingsEtree(bbbdomain=bbbdomain), internalMeetingID, 'meetingID')


def getMeetingModPWD(internalMeetingID: str, bbbdomain: str = bbbdomain) -> Optional[str]:
    ''' Lookup the moderator password for a meeting

    :param internalMeetingID: internalMeetingID of the meeting
    :type internalMeetingID: string
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: Moderator Password for a meeting
    :rtype: string

    '''

    return etreeMeetingTag(getMeetingsEtree(bbbdomain=bbbdomain), internalMeetingID, 'moderatorPW')


def getMeetingAttPWD(internalMeetingID: str, bbbdomain: str = bbbdomain) -> Optional[str]:
    ''' Lookup the attendee password for a meeting

    :param internalMeetingID: internalMeetingID of the meeting
    :type internalMeetingID: string
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: Attendee Password for a meeting
    :rtype: string

    '''

    return etreeMeetingTag(getMeetingsEtree(bbbdomain=bbbdomain), internalMeetingID, 'attendeePW')


_cacheFM: LRUCache = LRUCache(maxsize=16)
@cached(_cacheFM, key=cachefunc)
def findMeeting(meetingsList: list, needle: str) -> Optional[dict]:
    ''' Find a meeting by internalMeetingID, meetingID or meetingName

    :param meetingsList: list of meetings (as dicts)
    :type meetingsList: list
    :param needle: string to search for
    :type needle: string
    :results: dictionary for the requested meeting
    :rtype: dict

    '''

    for _ in range(0, len(meetingsList)):
        if needle in meetingsList[_].values():
            return meetingsList[_]
    return None

def getMeetingAttendees(internalMeetingID: str, bbbdomain: str = bbbdomain) -> Optional[list]:
    ''' Returns the attendees of a meeting

    :param internalMeetingID: internalMeetingID of the meeting
    :type internalMeetingID: string
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: attendees for meeting
    :rtype: list

    '''

    return etreeMeetingAttendees(getMeetingsEtree(bbbdomain=bbbdomain), internalMeetingID)


def getMeetingModerators(internalMeetingID: str, bbbdomain: str = bbbdomain) -> Optional[list]:
    ''' Returns the moderators of a meeting

    :param internalMeetingID: internalMeetingID of the meeting
    :type internalMeetingID: string
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: moderators for meeting
    :rtype: list

    '''

    attendees = getMeetingAttendees(internalMeetingID, bbbdomain=bbbdomain)
    if attendees:
        moderators = []
        for _ in attendees:
            if _['role'] == 'MODERATOR':
                moderators.append(_)
        return moderators
    return None


def isMeetingRunning(meetingid: str, idType: Optional[str] = None, bbbdomain: str = bbbdomain) -> Optional[bool]:
    ''' Ask the BBB server if a meeting is running.

    If the meeting is not running/found, the server returns nothing.
    As long as this is not fixed in the BBB-API in this case the result is always 'None'.

    :param meetingid: internalMeetingID, meetingID or meetingName
    :type meetingid: string
    :param idType: 'auto', 'meetingID'
                   In case 'meetingID' is given, we don't have to make an additional request to the BBB server to parse the whole lot of meetings.
    :type idType: string
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: True, False or if an error occurs None
    :rtype: bool, None

    '''

    if idType != 'meetingID':
        meeting: Optional[dict] = findMeeting(getMeetingsList(bbbdomain=bbbdomain), meetingid)
        if meeting:
            meetingid = meeting['meetingID']
        else:
            return None
    res = bbbApiRequest('isMeetingRunning', 'meetingID=' + meetingid, bbbdomain=bbbdomain)
    if res:
        if etreeIsMeetingRunning(res['etree']).lower() == 'true':
            return True
        #return False
    return None

def endMeetingUri(meetingid: str, bbbdomain: str = bbbdomain) -> Optional[str]:
    ''' End a meeting

    :param meetingid: internalMeetingID, meetingID or meetingName
    :param meetingid: string
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: XML response from BBB API
    :rtype: string

    '''

    meeting: Optional[dict] = findMeeting(getMeetingsList(bbbdomain=bbbdomain), meetingid)
    if meeting:
        params: dict = {}
        params['meetingID'] = meeting['meetingID']
        params['password'] = getMeetingModPWD(meeting['internalMeetingID'])
        return bbbApiRequestUri('end', bbbApiParamsDict2Str(params), bbbdomain=bbbdomain)

    return None


def endMeeting(meetingid: str, bbbdomain: str = bbbdomain) -> Optional[str]:
    ''' End a meeting

    :param meetingid: internalMeetingID, meetingID or meetingName
    :param meetingid: string
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: XML response from BBB API
    :rtype: string

    '''

    meeting: Optional[dict] = findMeeting(getMeetingsList(bbbdomain=bbbdomain), meetingid)
    if meeting:
        params: dict = {}
        params['meetingID'] = meeting['meetingID']
        params['password'] = getMeetingModPWD(meeting['internalMeetingID'])
        res = bbbApiRequest('end', bbbApiParamsDict2Str(params), bbbdomain)
        if res:
            return res['xml']

    return None


def _prepareJoinParams(meetingid: str, joinAs: str, fullName: str, params: dict, bbbdomain: str = bbbdomain) -> Optional[str]:
    ''' Prepares the parameters for joining a meeting.
    Default values from the config will be merged (overwritten) with given params dictionary.
    If meetingID is None, it will be created.
    If a meetingID is given in the params dictionary it will be taken only if the meetingID parameter is None

    :param meetingid: internalMeetingID, meetingID or meetingName for the meeting to join
    :type meetingid: string
    :param params: Dictionary of parameters for the BBB API request as specified by the BBB API
    :type params: dict
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: params as string
    :rtype: None,string

    '''

    meeting: Optional[dict] = findMeeting(getMeetingsList(bbbdomain=bbbdomain), meetingid)
    if meeting:
        params['fullName'] = fullName
        params['meetingID'] = meeting['meetingID']
        if joinAs == 'moderator':
            params['password'] = getMeetingModPWD(meeting['internalMeetingID'], bbbdomain=bbbdomain)
        elif joinAs == 'viewer':
            params['password'] = getMeetingAttPWD(meeting['internalMeetingID'], bbbdomain=bbbdomain)

        return bbbApiParamsDict2Str({**joinParamsDefaults, **params})

    return None


def joinMeetingUri(meetingid: str, joinAs: str, fullName: str, params: dict = {}, bbbdomain: str = bbbdomain) -> Optional[str]:
    ''' Join a meeting

    :param meetingid: internalMeetingID, meetingID or meetingName
    :type meetingid: string
    :param joinAs: 'moderator' or 'viewer'
    :type joinAs: str
    :param fullName: The full name that is to be used to identify this user to other conference attendees.
    :type fullName: str
    :param params: Dictionary of parameters for the BBB API request as specified by the BBB API
    :type params: None,dict
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: XML response from BBB API
    :rtype: string

    '''

    paramsstr: Optional[str] =  _prepareJoinParams(meetingid, joinAs, fullName, params, bbbdomain=bbbdomain)
    if paramsstr:
        return bbbApiRequestUri('join', paramsstr, bbbdomain=bbbdomain)

    return None


def joinMeeting(meetingid: str, joinAs: str, fullName: str, params: dict = {}, bbbdomain: str = bbbdomain) -> Optional[dict]:
    ''' Join a meeting

    :param meetingid: internalMeetingID, meetingID or meetingName
    :type meetingid: string
    :param joinAs: 'moderator' or 'viewer'
    :type joinAs: str
    :param fullName: The full name that is to be used to identify this user to other conference attendees.
    :type fullName: str
    :param params: Dictionary of parameters for the BBB API request as specified by the BBB API
    :type params: None,dict
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: XML response from BBB API
    :rtype: string

    '''

   
    paramsstr: Optional[str] =  _prepareJoinParams(meetingid, joinAs, fullName, params, bbbdomain=bbbdomain)
    if paramsstr:
        return bbbApiRequest('join', paramsstr, bbbdomain=bbbdomain)

    return None


def _prepareCreateParams(meetingID: Optional[str], params: dict) -> Optional[str]:
    ''' Prepares the parameters for creating a meeting.
    Default values from the config will be merged (overwritten) with given params dictionary.
    If meetingID is None, it will be created.
    If a meetingID is given in the params dictionary it will be taken only if the meetingID parameter is None

    :param meetingID: meetingID for the new meeting. Must be unique
    :type meetingID: None,string
    :param params: Dictionary of parameters for the BBB API request as specified by the BBB API
    :type params: dict
    :returns: XML response from BBB API
    :rtype: string

    '''

    if validateMeetingID(meetingID):
        params['meetingID'] = meetingID

    elif validateMeetingID(params.get('meetingID')):
        pass #params['meetingID'] = params['meetingID']        

    elif not meetingID and not params.get('meetingID'):
        params['meetingID'] = str(uuid4())

    else:
        return None

    # if no meetingName in params dict, set it to meetingID
    if not params.get('meetingName'):
        params['meetingName'] = params['meetingID']

    return bbbApiParamsDict2Str({**createParamsDefaults, **params})


def createMeetingUri(meetingID: Optional[str] = None, params: dict = {}, bbbdomain: str = bbbdomain) -> Optional[str]:
    ''' Create a meeting.
    If meetingID is None, it will be created.

    :param meetingID: meetingID for the new meeting. Must be unique
    :type meetingID: None,string
    :param params: Dictionary of parameters for the BBB API request as specified by the BBB API
    :type params: None,dict
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: XML response from BBB API
    :rtype: string

    '''

    paramsstr: Optional[str] =  _prepareCreateParams(meetingID, params)
    if paramsstr:
        return bbbApiRequestUri('create', paramsstr, bbbdomain=bbbdomain)

    return None


def createMeeting(meetingID: Optional[str] = None, params: dict = {}, bbbdomain: str = bbbdomain) -> Optional[dict]:
    ''' Create a meeting.
    If meetingID is None, it will be created.

    :param meetingID: meetingID for the new meeting. Must be unique
    :type meetingID: None,string
    :param params: Dictionary of parameters for the BBB API request as specified by the BBB API
    :type params: None,dict
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: XML response from BBB API
    :rtype: string

    '''

    paramsstr: Optional[str] =  _prepareCreateParams(meetingID, params)
    if paramsstr:
        return bbbApiRequest('create', paramsstr, bbbdomain=bbbdomain)

    return None
