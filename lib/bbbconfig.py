#!/usr/bin/python
''' Functions related to BBB config
'''

__author__ = 'Markus Gschwendt'
__copyright__ = 'See the file README for further information'
__license__ = 'See the file LICENSE for further information'

from typing import Optional
from cachetools import *
from .bbblib import cachefunc
from .config import bbbdomain
from .bbbapi import bbbApiRequest

_cacheDCXX: LRUCache = LRUCache(maxsize=16)
@cached(_cacheDCXX, key=cachefunc)
def getDefaultConfigXMLXml(bbbdomain: str = bbbdomain) -> Optional[str]:
    ''' Retreive the default config for meetings from the BBB server as XML

    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: XML for specified meeting
    :rtype: string

    '''

    res = bbbApiRequest('getDefaultConfigXML', '', bbbdomain=bbbdomain)
    if res:
        return res['xml']
    return None


_cacheDCXE: LRUCache = LRUCache(maxsize=16)
@cached(_cacheDCXE, key=cachefunc)
def getDefaultConfigXMLEtree(bbbdomain: str = bbbdomain):
    ''' Retreive the default config for meetings from the BBB server as etree

    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: etree object for specified meeting
    :rtype: object

    '''

    res = bbbApiRequest('getDefaultConfigXML', '', bbbdomain=bbbdomain)
    if res:
        return res['etree']
    return None
