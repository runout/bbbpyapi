#!/usr/bin/python
''' Parser functions for the XML returned by the BBB API
This handles etrees. Returning the content of tree leaves,...

https://docs.python.org/3/library/xml.etree.elementtree.html
'''

__author__ = 'Markus Gschwendt'
__copyright__ = 'See the file README for further information'
__license__ = 'See the file LICENSE for further information'

import sys #, xmltodict
import xml.etree.ElementTree as ET
from .config import KEYLISTS

def _etreeMeetingByIID(etree, internalMeetingID):
    ''' Parse an internalMeetingID in the given etree object and return as the meeting as etree object

    :param etree: etree to be parsed
    :type etree: object
    :param internalMeetingID: internalMeetingID to search for
    :type internalMeetingID: string
    :returns: Meeting as etree object
    :rtype: object

    '''

    return etree.find('.//meeting[internalMeetingID=\'' + internalMeetingID + '\']')


def etreeParse(xml):
    ''' Convert XML to etree object

    :param xml: XML to be parsed
    :type xml: string
    :returns: etree object from XML
    :rtype: object

    '''

    return ET.fromstring(xml)


def _indent(elem, level=0):
    i = "\n" + level*"  "
    j = "\n" + (level-1)*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + '  '
        if elem.text and (not elem.tail or not elem.tail.strip()):
            elem.tail = i
        if (not elem.text or not elem.text.strip()) \
           and (not elem.tail or not elem.tail.strip()):
            # node on the same level 
            elem.tail = i
        for subelem in elem:
            _indent(subelem, level+1)
        subelem.tail = i 
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i
    return elem


def etreeIndent(etree, level=0):
    return ET.dump(_indent(etree, level=level))


def _findElementTexts(elements, keys: list) -> dict:
    _dict = {}
    for _key in keys:
        _elemfound = elements.find('.//' + _key)
        try:
            _dict[_key] = _elemfound.text
        except:
            pass

    return _dict


def _parseSubtreeTexts(etree, treefilter: str, keys: list) -> list:
    res: list = []
    if etree:
        for _child in etree.findall(treefilter):
            _textsdict = _findElementTexts(_child, keys)
            if len(_textsdict) > 0:
                res.append(_textsdict)

    return res


def etreeStatus(etree):
    return _findElementTexts(etree, KEYLISTS['STATUS'])


def etreeOrigin(etree, internalMeetingID):
    return _parseSubtreeTexts(etree, internalMeetingID, KEYLISTS['ORIGIN'])


def etreeMeetingsList(etree):
    return _parseSubtreeTexts(etree, './/meeting', KEYLISTS['MEETING'])


def etreeIsMeetingRunning(etree):
    return etree.find('.//running').text


def etreeMeetingAttendees(etree, internalMeetingID):
    _subtree = _etreeMeetingByIID(etree, internalMeetingID)
    return _parseSubtreeTexts(_subtree, './/attendee', KEYLISTS['ATTENDEE'])


def etreeMeetingPWD(meetings, internalMeetingID):
    return _etreeMeetingByIID(meetings, internalMeetingID).find('moderatorPW').text


def etreeMeetingTag(meetings, internalMeetingID, tagName):
        return _etreeMeetingByIID(meetings, internalMeetingID).find('.//' + tagName).text


def etreeCount(meetings, internalMeetingID, tagName):
        return _etreeMeetingByIID(meetings, internalMeetingID).find(tagName).text


def etreeRecordingsList(etree):
    return _parseSubtreeTexts(etree, './/attendee', KEYLISTS['RECORDING'])
