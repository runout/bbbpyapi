''' Config parser
'''

__author__ = 'Markus Gschwendt'
__copyright__ = 'See the file README for further information'
__license__ = 'See the file LICENSE for further information'

import sys
import os.path as path
from json import loads
from configparser import ConfigParser

if not path.isfile('config.ini'):
    print ("ERROR: File config.ini is missing\n")
    sys.exit(3)

config = ConfigParser()
config.read(['config_defaults.ini', 'config.ini'])

bbbdomain: str = config.get('server', 'bbbdomain')

BBBSECRET: str = config.get('server', 'BBBSECRET')

timeMaxUnmoderated: int = config.getint('local', 'timeMaxUnmoderated')
fileLog = config.get('local', 'fileLog')

fileWatchUnmoderated: str = path.join(config.get('local', 'dirVarRun'), config.get('local', 'fileWatchUnmoderated'))

createParamsDefaults: dict = dict(config.items('createparams'))
joinParamsDefaults: dict = dict(config.items('joinparams'))

# values from section keylists
KEYLISTS: dict = {}
for _k, _v in config.items('keylists'):
    KEYLISTS[_k.upper()] = loads(_v)
