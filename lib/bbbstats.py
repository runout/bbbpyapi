#!/usr/bin/python
''' Statistics for BBB meetings
'''

__author__ = 'Markus Gschwendt'
__copyright__ = 'See the file README for further information'
__license__ = 'See the file LICENSE for further information'

#from os import dir
from typing import Optional, Union
from tabulate import tabulate, _table_formats # type: ignore
from .config import KEYLISTS, bbbdomain
from .bbbmeetings import getMeetingsList
from .bbbmeeting import getMeetingModerators, getMeetingStats

table_formats = [*_table_formats]
table_formats.append('dict')
table_formats.append('e_html')


class TableFormatter:
    ''' Functions for formatting a table
    '''

    def cellFormatter(self, cellContent: Union[int, str], colName: str, cellStyle: str, fmt: str, params: Optional[dict] = None) -> Union[int, str]:
        ''' Formatter for cell contents.

        :param cellContent: content/value of the cell
        :type cellContent: string
        :param colName: name of column to format
        :type colName: string
        :param fmt: type of output format
        :type fmt: string
        :param params: dictionary of parameters needed for rendering
        :type params: dict
        :returns: formatted cell
        :rtype: int, string

        '''

        newCellContent = cellContent

        if fmt == 'e_html':
            style: str = ''
            if cellStyle == 'footer':
                 style = style + 'border-top: 1px solid black; '
            if isinstance(cellContent, int):
                 style = style + 'text-align: right; '
                 cellContent = str(cellContent)

            newCellContent = '<td>' + cellContent + '</td>'
            # cell of header line
            if cellStyle == 'header':
                newCellContent = '<th>' + cellContent + '</th>'
            else:
                if style != '':
                     style = ' style="' + style + '"'

                if colName == 'moderator names':
                    cellContent = cellContent.replace(';', '<br />')

                if colName == 'duration' and cellStyle == 'footer':
                    newCellContent = '<td' + style + '></td>'
                else:
                    newCellContent = '<td' + style + '>' + cellContent + '</td>'

        return newCellContent


    def rowFormatter(self, rowContent: list, rowStyle: str, fmt: str, params: Optional[dict] = None) -> Union[list, str]:
        ''' Formatter for row contents.

        :param rowContent: list of the cells for the row
        :type rowContent: list
        :param rowStyle: type of row: 'header', 'body', 'footer'
        :type rowStyle: string
        :param fmt: type of output format
        :type fmt: string
        :param params: dictionary of parameters needed for rendering
        :type params: dict
        :returns: formatted row
        :rtype: list, string

        '''

        newRowContent: Union[list, str] = rowContent

        if fmt == 'e_html':
            newRowContent = ''.join(_cell for _cell in rowContent)

            style: str
            # cell of header line
            if rowStyle == 'header':
        
                style = 'text-align: center; font-weight: bold; '

                if style != '':
                    style = ' style="' + style + '"'

                newRowContent = '<tr' + style + '>' + newRowContent + '</tr></thead><tbody>'

            elif rowStyle == 'footer':
                style = 'font-weight: bold; '

                if style != '':
                    style = ' style="' + style + '"'

                newRowContent = '</tbody><tfoot><tr' + style + '>' + newRowContent + '</tr>'
            else:
                newRowContent = '<tr>' + newRowContent + '</tr>'

        return newRowContent


    def tableFormatter(self, tableContent: list, fmt: str, params: Optional[dict] = None) -> Union[list, str]:
        ''' Formatter for table contents.

        :param tableContent: list of rows for the table
        :type tableContent: list
        :param fmt: type of output format
        :type fmt: string
        :param params: dictionary of parameters needed for rendering
        :type params: dict
        :returns: formatted table
        :rtype: list, string

        '''

        newTableContent: Union[list, str] = tableContent

        if fmt == 'e_html':
            newTableContent = ''.join(_row for _row in tableContent)
            newTableContent = '<table><thead>' + newTableContent + '</tfoot></table>'

        return newTableContent


def getStats(meetingList: Optional[list] = None, fmt: str = 'simple', tableFormatter = None, bbbdomain: str = bbbdomain) -> Union[None, dict, str]:
    ''' Returns some statistics for all meetings in the given list

    :param meetingList: list of BBB meetings
    :type meetingList: list of meetings (as dict)
    :param fmt: format which is undersood by tabulate (simple, plain, html, ...)
                or a private format, dendered in the ...FormatterFunction functions
                or a dict of lists. Keys are 'header', 'total' and the meetingName, 'moderator names', 'origin', '', ''
    :type fmt: string
    :param bbbdomain: Domain name of the BBB server defaults to variable bbbdomain from config file
    :type bbbdomain: string
    :returns: formatted statistic
    :rtype: string

    '''

    if not meetingList:
        meetingList = getMeetingsList(bbbdomain=bbbdomain)

    # if there are no meetings return None
    if not meetingList:
        return None

    if not tableFormatter:
        tableFormatter = TableFormatter()

    
    #columns: list = KEYLISTS['STATS_METRIC_COLS']
    #columns_all: list = KEYLISTS['STATS_OUT_COLS'] # headline
    table: dict = {}
    row: list = []
    cellparams: dict = {'bbbdomain': bbbdomain}
    rowparams: Optional[dict] = {}
    tableparams: Optional[dict] = {}

    # create head line
    for _col in KEYLISTS['STATS_OUT_COLS']: # headline
        row.append(tableFormatter.cellFormatter(_col, _col, 'header', fmt, params=cellparams))
    table['header'] = tableFormatter.rowFormatter(row, 'header', fmt, params=rowparams)

    total: dict[str, int] = {} # first column

    for _col in KEYLISTS['STATS_METRIC_COLS']:
        total[_col] = 0

    for meeting in meetingList:
        intMeetID = meeting['internalMeetingID']
        meetingStats = getMeetingStats(intMeetID, bbbdomain=bbbdomain)
        cellparams['meetingID'] = intMeetID

        if meetingStats:
            row = [tableFormatter.cellFormatter(str(meeting['meetingName']), 'meeting', 'body', fmt, params=cellparams)] # first column

            for _col in KEYLISTS['STATS_METRIC_COLS']:

                if _col == 'duration':
                    duration: int = int(meetingStats[_col] / 60)
                    total[_col] = total[_col] + duration
                    row.append(tableFormatter.cellFormatter(duration, 'duration', 'body', fmt, params=cellparams))
                else:
                    total[_col] = total[_col] + meetingStats[_col]
                    row.append(tableFormatter.cellFormatter(meetingStats[_col], _col, 'body', fmt, params=cellparams))

            # column recording
            #row.append(tableFormatter.cellFormatter(meeting['recording'], 'recording', fmt))

            # column moderators (names)
            if getMeetingModerators(intMeetID, bbbdomain=bbbdomain):
                _cell = '; '.join(str(_['fullName']) for _ in getMeetingModerators(intMeetID, bbbdomain=bbbdomain)) # type: ignore
            else:
                _cell = ''
            row.append(tableFormatter.cellFormatter(_cell, 'moderator names', 'body', fmt, params=cellparams)) # type: ignore

            # column origin-server
            row.append(tableFormatter.cellFormatter(meeting['bbb-origin-server-name'], 'origin', 'body', fmt, params=cellparams))
            #table[meeting['meetingName']] = tableFormatter.rowFormatter(row, 'body', fmt)

            # column join meeting
            row.append(tableFormatter.cellFormatter('Join', 'join meeting', 'body', fmt, params=cellparams))
            #table[meeting['meetingName']] = tableFormatter.rowFormatter(row, 'body', fmt)

            # column end meeting
            row.append(tableFormatter.cellFormatter('End', 'end meeting', 'body', fmt, params=cellparams))

            # add row to table
            table[meeting['meetingName']] = tableFormatter.rowFormatter(row, 'body', fmt, params=rowparams)

    del cellparams['bbbdomain']
    # footer (total)
    row = [tableFormatter.cellFormatter('Total', 'meeting', 'footer', fmt, params=cellparams)] # first column
    for _col in KEYLISTS['STATS_METRIC_COLS']:
        row.append(tableFormatter.cellFormatter(total[_col], _col, 'footer', fmt, params=cellparams))
    for _ in range(len(KEYLISTS['STATS_OUT_COLS']) - len(row)):
        row.append(tableFormatter.cellFormatter('', '', 'footer', fmt, params=cellparams))
    table['total'] = tableFormatter.rowFormatter(row, 'footer', fmt, params=rowparams)

    # use tabulate formatting if fmt is in tabulate format list
    table_out: Union[None, str, dict]
    if fmt in _table_formats:
        table_out = tabulate(table.values(), tablefmt=fmt, headers='firstrow')
    elif fmt == 'dict':
        table_out = table
    else:
        table_out = tableFormatter.tableFormatter(table.values(), fmt, params=tableparams)

    return table_out
