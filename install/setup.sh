#!/bin/bash

#__author__ = 'Markus Gschwendt'
#__copyright__ = 'See the file README for further information'
#__license__ = 'See the file LICENSE for further information'

DIR_ORIG="$(PWD)"
cd $( dirname "${BASH_SOURCE[0]}" )/../

python3 -V
echo "python3 must at least be 3.6"
# install python3 plugins
apt-get -y install python3-cachetools python3-dateutil python3-click python3-tabulate

# for building docu
apt-get -y python3-sphinx python3-sphinx-rtd-theme rst2pdf
 
# for making stats available via http
apt-get -y install uwsgi uwsgi-plugin-python3 python3-venv

# create virtal environment for uwsgi
python3 -m venv venv
. venv/bin/activate
pip3 install click cachetools requests python-dateutil tabulate
#pip3 install -r requirements.txt
deactivate

cd ${DIR_ORIG}
