#!/bin/bash

#__author__ = 'Markus Gschwendt'
#__copyright__ = 'See the file README for further information'
#__license__ = 'See the file LICENSE for further information'

sphinx-build -b html docs/source/ docs/build/html/
#sphinx-build -b pdf docs/source/ docs/build/pdf/
