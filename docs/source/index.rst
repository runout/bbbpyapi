.. BBB API for Python documentation master file, created by
   sphinx-quickstart on Wed Feb 10 00:23:15 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BBB API for Python's documentation!
==============================================

Contents:

.. toctree::
   :maxdepth: 2

bbbstats.py
===================
.. automodule:: bbbstats
  :members:

bbbcleaner.py
===================
.. automodule:: bbbcleaner
  :members:

wsgi
===================
.. automodule:: wsgi
  :members:

bbbpyapi
===================
.. automodule:: bbbpyapi
  :members:

bbbstats
===================
.. automodule:: lib.bbbstats
  :members:

config
===================
.. automodule:: lib.config
  :members:

bbbmeetings
===================
.. automodule:: lib.bbbmeetings
  :members:

bbbmeeting
===================
.. automodule:: lib.bbbmeeting
  :members:

bbbapi
===================
.. automodule:: lib.bbbapi
  :members:

bbbparse
===================
.. automodule:: lib.bbbparse
  :members:

bbbtasks
===================
.. automodule:: lib.bbbtasks
  :members:

bbblib
===================
.. automodule:: lib.bbblib
  :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

