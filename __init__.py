__author__ = 'Markus Gschwendt'
__version__ = "0.0.3"
__copyright__ = 'See the file README.md for further information'
__license__ = 'See the file LICENSE for further information'
__credits__ = []
__maintainer__ = "Markus Gschwendt"
__email__ = "dev+bbbpyapi@runout.at"
__status__ = "Development"
