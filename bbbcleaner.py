#!/usr/bin/python
''' Ends BBB meetings afer an amount of time.

Command line utility.
'''

__author__ = 'Markus Gschwendt'
__copyright__ = 'See the file README.md for further information'
__license__ = 'See the file LICENSE for further information'

import click
from lib.config import bbbdomain
from lib.bbbtasks import cleanupUnmoderated



@click.command()
@click.option('-t', '--timeout', type=int, help='After this amount of seconds an unmoderated meeting will be ended.')
@click.option('-d', '--domain', type=str,
      help='''BBB Domain/Server''')
def main_cleanupUnmoderated(timeout, domain):

    if not domain:
        domain = bbbdomain

    if timeout:
        print(cleanupUnmoderated(timeOut=timeout, bbbdomain=domain))
    else:
        print(cleanupUnmoderated(bbbdomain=domain))




if __name__ == "__main__":
    main_cleanupUnmoderated()
