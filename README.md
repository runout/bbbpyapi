# bbbpyapi - BBB API for Python

Provides functions to interact with the [API of a BBB](https://docs.bigbluebutton.org/dev/api.html) server.

Some function results will be cached. See the [Documentation](#caching) section below.

## Available API-functions

- create
- join
- isMeetingRunning
- end
- getMeetingInfo
- getMeetings
- getRecordings
- getDefaultConfigXML

## Functionality

- Return API-call results as XML, etree or dictionary
- Retreive statistics as dictionary, formatted in many ways (tabulate) or flexible by your own formatter
- End meetings without moderators after specified time

## Install

See the _install_ directory for details. There is a _setup.sh_ script to install some dependencies.

Create a _config.ini_ file and set _bbbdomain_ in the _[server]_ section. The BBBSECRET is necessary if the program is not running on the BBB server. For an example see the _config_defaults.ini_ file, which will be included before the _config.ini_ file

## Usage

Following programs are examples for using the _bbbpyapi_ library.

### bbbstats.py

This program returns some useful statistics. Run _bbbstats.py --help_ for details.

### bbbcleaner.py

Ends meetings which are unmoderated for a certain time.

-t, --timeout defines the time after which a meeting will be ended.

Defaults are configured in the .ini files. 

### bbbpyapi.py

This is a python module which provides the most common functions.

### wsgi.py

For usage with uwsgi a sample uwsgi config is provided in the _install/uwsgi_ subdirectory. And a nginx config snippet in _install/nginx_.

To include this in an other website just write something like `<object data="https://bbbstats.example.com/" type="text/html" height="200" width="100%"></object>` into the html of that site.

## Documentation

You can build the documentation (Sphinx) with the script _build-docs.sh_. Output goes to _docs/build/html/_. If you want a different format, install the needed modules and change or add the command in the _build-docs.sh_ script.

Then point your webserver to the generated files.

### Caching

Some function results will be cached. It is safe to call these functions multiple times with the same arguments during a program run or in a single web request without actually running the code again and getting different results. For calculating the hash, additionally to the function arguments, a timestamp from the program/wsgi start will be used and on wsgi some parameters from the request (lib/bbblip.py).

## Copyright

Copyright (c) 2021 by Markus Gschwendt

## License

See the LICENSE file which should be provided along with this program for further information.

## Author

Ing. Markus Gschwendt
