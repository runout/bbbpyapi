#!/bin/bash

#__author__ = 'Markus Gschwendt'
#__copyright__ = 'See the file README for further information'
#__license__ = 'See the file LICENSE for further information'

for PY in bbbpyapi.py bbbstats.py bbbcleaner.py; do
    echo -e "\n##### testing ${PY} #####"
    echo " ...with mypy --namespace-packages --explicit-package-bases ${PY}"
    mypy --namespace-packages --explicit-package-bases ${PY}
done
