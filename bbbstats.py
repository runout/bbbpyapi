#!/usr/bin/python
''' Generates statistcs for a BBB server.

Command line utlity.
'''

__author__ = 'Markus Gschwendt'
__copyright__ = 'See the file README.md for further information'
__license__ = 'See the file LICENSE for further information'

import click
from typing import Optional, Union
from lib.config import config, bbbdomain

from lib.bbbmeetings import getMeetingsList, getMeetingsXml
from lib.bbbmeeting import isMeetingRunning, getMeetingID, endMeetingUri, joinMeetingUri, createMeetingUri
from lib.bbbrecordings import getRecordingsList, getRecordingsXml, getRecordingsEtree
from lib.bbbconfig import getDefaultConfigXMLXml
from lib.bbbstats import TableFormatter, getStats, table_formats


@click.command()
@click.option('-l', '--list-meetings', is_flag=True,
      help='list meetings')
@click.option('--stats', is_flag=True, default=False,
      help='some statistics for meetings')
@click.option('--fmt',
      type=click.Choice(table_formats), default='simple',
      help='Type of output formatting')
@click.option('--xml', is_flag=True,
      help='output from bbb api "getMeetings" as xml')
@click.option('--show-config', is_flag=True,
      help='Show config as parsed from files')
@click.option('--running', type=str,
      help='''Check if meeting is running.
            Value of TEXT can be the name, meetingID or internalMeetingID''')
@click.option('--idtype',
      type=click.Choice(['auto', 'meetingName', 'internalMeetingID', 'meetingID']),
      default='auto',
      help='Type of given meeting ID')
@click.option('--bbb-default-config', is_flag=True,
      help='Get default config for meetings from BBB server')
@click.option('-r', '--list-recordings', is_flag=True,
      help='list recordings. Filter by (--mid or --rid) and --state and --meta')
@click.option('--mid', type=str,
      help='''meetingID''')
@click.option('--rid', type=str,
      help='''recordID''')
@click.option('--state', type=str,
      help='''State of recording''')
@click.option('--meta', type=str,
      help='''Metadata of recording''')
@click.option('--join-moderator', type=str,
      help='''Join a meeting.
            Value of TEXT can be the name, meetingID or internalMeetingID''')
@click.option('--create', is_flag=True,
      help='''Create a meeting with --mid meetingID.
            If --mid meetingID is not given, it will be auto-created''')
@click.option('-d', '--domain', type=str,
      help='''BBB Domain/Server''')
def main(list_meetings, stats, fmt, xml, show_config, running, idtype, bbb_default_config, list_recordings, mid, rid, state, meta, join_moderator, create, domain):
    ''' Main function

    :param: options as defined in decorators

    '''

    if not domain:
        domain = bbbdomain

    if list_meetings:
        if xml:
            print(getMeetingsXml(bbbdomain=domain))
        else:
            print(getMeetingsList(bbbdomain=domain))
            
    elif stats:
        print(getStats(fmt=fmt, tableFormatter=MyTableFormatter(), bbbdomain=domain))

    elif show_config:
        for _s in config.sections():
            print('\n[', _s, ']')
            for _c in config[_s]:
                print(_c, ' = ', config[_s][_c])

    elif running:
        print(isMeetingRunning(running, idtype, bbbdomain=domain))

    elif bbb_default_config:
        print(getDefaultConfigXMLXml(bbbdomain=domain))

    elif list_recordings:
        meta_dict = {}
        if meta:
            meta_dict = {_m.partition('=')[0]: _m.partition('=')[2] for _m in meta.split(',')}

        if xml:
            print(getRecordingsXml({'meetingID': mid, 'recordID': rid, 'state': state, 'meta': meta_dict}, bbbdomain=domain))
        else:
            print(getRecordingsList(getRecordingsEtree({'meetingID': mid, 'recordID': rid, 'state': state, 'meta': meta_dict}, bbbdomain=domain)))

    elif join_moderator:
        print(joinMeetingUri(join_moderator, 'moderator', 'IT Admin', bbbdomain=domain))

    elif create:
        print(createMeetingUri(mid, bbbdomain=domain))

    else:
        print(getStats(getMeetingsList(bbbdomain=domain), bbbdomain=domain))


class MyTableFormatter(TableFormatter):
    def cellFormatter(self, cellContent: Union[int, str], colName: str, cellStyle: str, fmt: str, params: Optional[dict] = None) -> Union[int, str]:
        ''' Formatter for cell contents.

        :param cellContent: content/value of the cell
        :type cellContent: string
        :param colName: name of column to format
        :type colName: string
        :param fmt: type of output format
        :type fmt: string
        :param params: dictionary of parameters needed for rendering
        :type params: dict
        :returns: formatted cell
        :rtype: int, string

        '''

        newCellContent = cellContent
        if fmt == 'e_html':
            style: str = ''
            if cellStyle == 'footer':
                 style = style + 'border-top: 1px solid black; '
            if isinstance(cellContent, int):
                 style = style + 'text-align: right; '
                 cellContent = str(cellContent)

            newCellContent = '<td>' + cellContent + '</td>'
            # cell of header line
            if cellStyle == 'header':
                newCellContent = '<th>' + cellContent + '</th>'
            else:
                if style != '':
                     style = ' style="' + style + '"'

                if colName == 'moderator names':
                    cellContent = cellContent.replace(';', '<br />')

                elif colName == 'duration' and cellStyle == 'footer':
                    cellContent = '<td' + style + '></td>'

                elif colName == 'join meeting':
                    if params and params['meetingID']:
                        uriStr = joinMeetingUri(str(params['meetingID']), 'moderator', 'IT Admin', bbbdomain=params['bbbdomain'])
                        cellContent = '<a href="' + str(uriStr)  + '">' + cellContent + '</a>'
                    else:
                        cellContent = ''

                elif colName == 'end meeting':
                    if params and params['meetingID']:
                        uriStr = endMeetingUri(params['meetingID'])
                        cellContent = '<a href="' + str(uriStr)  + '">' + cellContent + '</a>'
                    else:
                        cellContent = ''

                newCellContent = '<td' + style + '>' + cellContent + '</td>'

        return newCellContent


    def tableFormatter(self, tableContent: list, fmt: str, params: Optional[dict] = None) -> Union[list, str]:
        ''' Formatter for table contents.

        :param tableContent: list of rows for the table
        :type tableContent: list
        :param fmt: type of output format
        :type fmt: string
        :param params: dictionary of parameters needed for rendering
        :type params: dict
        :returns: formatted table
        :rtype: list, string

        '''

        newTableContent: Union[list, str] = tableContent

        if fmt == 'e_html':
            style = 'style="background-color: #cccccc; "'
            newTableContent = ''.join(_row for _row in tableContent)
            newTableContent = '<table' + style + '>' + newTableContent + '</table>'

        return newTableContent



if __name__ == "__main__":
    main()
